<?php

/**
 * @file
 * Integrates Drupal's ad module with the MaxMind GeoIP database, providing 
 * GeoTargeting of advertisements.
 *
 * Sponsored by Pricescope.com.
 *
 * Copyright (c) 2008.
 *   Jeremy Andrews <jeremy@kerneltrap.org>.  All rights reserved.
 */

/**
 * Implementation of hook_requirements.  Be sure that the geoip package is
 * properly installed.
 */
function ad_geoip_requirements($phase) {
  if (function_exists('geoip_db_get_all_info')) { 
    $versions = array();
    // We currently support the following MaxMind databases:
    foreach (array(
               GEOIP_COUNTRY_EDITION => ' GeoIP Country Edition', 
               GEOIP_CITY_EDITION_REV0 => ' GeoIP City Edition, Rev 0', 
               GEOIP_CITY_EDITION_REV1 => ' GeoIP City Edition, Rev 1', 
               GEOIP_REGION_EDITION_REV0 => ' GeoIP Region Edition, Rev 0', 
               GEOIP_REGION_EDITION_REV1 => ' GeoIP Region Edition, Rev 1'
             ) as $edition => $name) {
      if (geoip_db_avail($edition)) {
        $version = geoip_database_info($edition);
        if ($version) {
          $version = split(' ', $version);
          $versions[] = $version[0] .' '. $version[1] . $name;
        }
      }
    }
    if (!empty($versions)) {
      $severity = REQUIREMENT_OK;
      $message = implode('; ', $versions);
    }
    else {
      $severity = REQUIREMENT_ERROR;
      $message = t('No valid GeoIP database installed.');
    }
  }
  else {
    $severity = REQUIREMENT_ERROR;
    $message = t('Required !extension version 1.0.1 or greater not installed.', array('!extension' => l(t('GeoIP extension'), 'http://pecl.php.net/package/geoip')));
  }

  return array(
    'geoip' => array(
      'title' => t('GeoIP'),
      'value' => $message,
      'severity' => $severity,
    ),
  );
}

/**
 * Drupal hook_form_alter() implementation.
 */
function ad_geoip_form_alter($form_id, &$form) {
  if ($form_id == 'ad_node_form') {
    $form['geoip'] = array(
      '#type' => 'fieldset',
      '#title' => t('GeoTargeting'),
      '#collapsible' => TRUE,
    );

    if (is_object($form['#node'])) {
      $node = $form['#node'];
    }
    $geoip_format = isset($node->geoip_format) ? $node->geoip_format : 0;
    $geoip_codes = isset($node->geoip_codes) ? $node->geoip_codes : array();

    $formats = array( 
      0 => t('all visitors'),
      1 => t('visitors from the countries selected below'),
      2 => t('visitors not from the countries selected below'),
    );
    $form['geoip']['geoip_format'] = array(
      '#type' => 'radios',
      '#title' => t('Display advertisements to'),
      '#options' => $formats,
      '#default_value' => $geoip_format,
    );

    $form['geoip']['geoip_codes'] = array(
      '#type' => 'select',
      '#options' => ad_geoip_countries(),
      '#multiple' => TRUE,
      '#default_value' => $geoip_codes,
      '#description' => t('If you select <em>all visitors</em> above, or you do not select any countries from this list, your advertisement will be displayed to all visitors.'),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }
}

/** 
 * Drupal hook_nodeapi implementation.  Save advertisement geotargeting
 * configuration.
 */
function ad_geoip_nodeapi(&$node, $op, $teaser, $page) {
  // TODO: Node versioning support.
  switch ($op) {
    case 'load':
      $result = db_query('SELECT code, format FROM {ad_geoip_ads} WHERE aid = %d', $node->nid);
      while ($code = db_fetch_object($result)) {
        $geoip_codes[] = $code->code;
        $format = $code->format;
      }
      if (isset($format)) {
        $ad['geoip_codes'] = $geoip_codes;
        $ad['geoip_format'] = $format;
        return $ad;
      }
      break;

    case 'update':
      db_query('DELETE FROM {ad_geoip_ads} WHERE aid = %d', $node->nid);
      // fall through and insert updated geoip information...
    case 'insert':
      if (is_array($node->geoip_codes)) {
        foreach($node->geoip_codes as $code) {
          db_query("INSERT INTO {ad_geoip_ads} (aid, code, format) VALUES(%d, '%s', %d)", $node->nid, $code, $node->geoip_format);
        }
      }
      break;

    case 'delete':
      db_query('DELETE FROM {ad_geoip_ads} WHERE aid = %d', $node->nid);
      break;
  }
}

/**
 * Define ad mdule hook_adapi().
 */
function ad_geoip_adapi($op, &$node) {
  switch ($op) {
    case 'adserve_select':
      return array(
        'geoip' => array(
          'function' => 'ad_geoip_adserve',
          'path' => drupal_get_path('module', 'ad_geoip') .'/ad_geoip.inc',
          'weight' => -10,
        ),
      );
  }
}

/**
 * Return an array of countries indexed on their country code.
 */
function ad_geoip_countries() {
  static $codes = array();

  if (empty($codes)) {
    // Automatically update our ISO country codes every 30 days.
    if ((int)variable_get('ad_geoip_iso3166', '') <= (86400 * 30)) {
      ad_geoip_update_iso3166();
    }

    $result = db_query('SELECT code, country FROM {ad_geoip_codes}');
    while ($code = db_fetch_object($result)) {
      $codes[$code->code] = decode_entities($code->country);
    }
  }

  return $codes;
}

/**
 * Download the latest ISO3166 list of country codes.
 */
function ad_geoip_update_iso3166($verbose = FALSE) {
  // url for latest iso3166 list of two-character country codes.
  $url = 'http://www.iso.org/iso/iso3166_en_code_lists.txt';
  // load list into an array.
  $codes = file($url);

  if (is_array($codes) && !empty($codes)) {
    $checksum = md5(implode(' ', $codes));
    if ($checksum != variable_get('ad_geoip_iso3166_checksum', '')) {
      // We expect the second line of the file to be essentially empty.  If not, 
      // the format has changed.
      if (strlen($codes[1] < 5)) {
        db_query('TRUNCATE {ad_geoip_codes}');
        $line = 2;
        while (!empty($codes[$line])) {
          list($country, $code) = split(';', $codes[$line++]);
          $country = check_plain(utf8_encode(ucwords(strtolower($country))));
          // Manual capitalization cleanup.
          $country = strtr($country, array(' And ' => ' and ', ' Of' => ' of', '(keeling)' => '(Keeling)', '(malvinas)' => '(Malvinas)', '(vatican' => '(Vatican', 'U.s.' => 'U.S.'));
          $code = trim($code);
          if (strlen($code) != 2) {
            if ($verbose) drupal_set_message(t('ISO3166 import error: invalid country code %code for country %country.', array('%code' => $code, '%country' => $country)), 'error');
          }
          else {
            db_query("INSERT INTO {ad_geoip_codes} (code, country) VALUES('%s', '%s')", $code, $country);
          }
        }
        if ($verbose) drupal_set_message('Your ISO3166 country codes have been updated.');
        // Store checksum to prevent reloading data if unchanged.
        variable_set('ad_geoip_iso3166_updated', time());
        variable_set('ad_geoip_iso3166_checksum', $checksum);
      }
      else {
        if ($verbose) drupal_set_message(t('The format of the file at !url has changed.  Check to see if there has been a !release of the Ad GeoIP module.', array('!url' => l($url, $url), '!release' => l(t('new release'), 'http://drupal.org/project/ad_geoip'))));
      }
    }
  }
  else {
    if ($verbose) drupal_set_message(t('Failed to retrieve country codes from !url.', array('!url' => l($url, $url))), 'error');
  }
}
